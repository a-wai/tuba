tuba (0.5.0+ds-1) unstable; urgency=medium

  * debian: add gbp.conf and switch to debian/latest branch
  * debian: exclude screenshots to fix packaging workflow.
    Upstream stores screenshots with git-lfs, so the resulting tarball only
    includes LFS metadata instead of the actual image files. This causes an
    issue with how we use `git-buildpackage` as it then generates an invalid
    orig tarball.
    In order to work around this issue, we can exclude those files from the
    orig tarball (effectively repacking upstream sources), as they aren't
    installed anyway.
  * d/debian.yml: link to canonical pipeline location
  * d/control: update for new version.
    Upstream bumped the required versions of some library dependencies, so
    should we. While at it, change the following:
    * update the `Vcs-*` links so they point to the actual repo location
    * add myself to `Uploaders` as I'm willing to co-maintain this package
    * drop the version requirement on `valac` (current version is 0.56)
  * d/patches: drop upstreamed patch

 -- Arnaud Ferraris <aferraris@debian.org>  Tue, 03 Oct 2023 16:07:42 +0200

tuba (0.2.0-3) experimental; urgency=medium

  [ Andres Salomon ]
  * Update for 0.2.0.
  * Add build-dep on libgtksourceview-5-dev.
  * Switch from using libsoup2.4-dev to libsoup-3.0-dev for build.

  [ Federico Ceratto ]
  * Initial release. (Closes: #1033373)

 -- Federico Ceratto <federico@debian.org>  Thu, 25 May 2023 20:51:06 +0100
